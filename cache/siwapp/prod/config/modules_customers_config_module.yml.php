<?php
// auto-generated by sfDefineEnvironmentConfigHandler
// date: 2015/10/16 12:20:55
sfConfig::add(array(
  'mod_customers_enabled' => true,
  'mod_customers_view_class' => 'sfPHP',
  'mod_customers_urls_include' => array (
  0 => 'search',
),
  'mod_customers_urls_variables' => array (
  'editRow' => 'customers/edit',
  'showInvoices' => 'customers/showInvoices',
),
));
